package com.easy.deploy.impl;

import com.easy.annotation.tag;
import com.easy.deploy.abs.DeployBuilder;
import com.easy.deploy.util.LogUtil;
import com.easy.deploy.util.NpmUtil;
import com.easy.deploy.util.SftpUtil;
import com.easy.enums.EasyJenkinsEnum;

import java.io.IOException;

/**
 * @author tanyongpeng
 * <p>des</p>
 **/
@tag(name = "vue")
public class VueBuilder extends DeployBuilder {

    @Override
    public void npmBuild() {
        LogUtil.info("开始构造vue项目",deployConnect.getConnectId(),deployConnect.getHost());
        try {
            NpmUtil.runExecution(deployConnect.getVueRootLocalPath(),"npm run build");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        LogUtil.info("vue项目构造完成",deployConnect.getConnectId(),deployConnect.getHost());
    }

    @Override
    public void delete() throws IOException {
        LogUtil.info("开始删除远程的文件",deployConnect.getConnectId(),deployConnect.getHost());
        SftpUtil.exec(
                deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(),
                "rm -rf "+deployConnect.getServerPath());
        LogUtil.info("删除完成",deployConnect.getConnectId(),deployConnect.getHost());
    }

    @Override
    public void initDeployPath() {
        LogUtil.info("初始化上传路径",deployConnect.getConnectId(),deployConnect.getHost());
        SftpUtil.getDirectory(deployConnect.getLocalPath());
        SftpUtil.getDirectory(deployConnect.getLocalPath(),deployConnect.getServerPath());
        deployPath = SftpUtil.deployPath;
        LogUtil.info("路径初始完成",deployConnect.getConnectId(),deployConnect.getHost());
    }

    @Override
    public void mkdir() throws IOException {
        LogUtil.info("开始创建文件夹",deployConnect.getConnectId(),deployConnect.getHost());
        StringBuilder pathAll = new StringBuilder();
        for (String path : deployPath.getDirServerList()) {
            if (pathAll.toString().equals("")){
                pathAll = new StringBuilder(path);
            }else {
                pathAll.append(",").append(path);
            }
        }
        SftpUtil.exec(
                deployConnect.getHost(),
                deployConnect.getPort(),
                deployConnect.getUsername(),
                deployConnect.getPassword(),
                "mkdir -p "+deployPath.getRootPath()+"/{"+pathAll.toString()+"}");
        LogUtil.info("创建完成",deployConnect.getConnectId(),deployConnect.getHost());
    }

    @Override
    public void uploading() {
        LogUtil.info("开始批量上传...",deployConnect.getConnectId(),deployConnect.getHost());
        SftpUtil.batchUploadFile(deployConnect.getHost(),deployConnect.getPort(),
                deployConnect.getUsername(),deployConnect.getPassword(),deployPath.getUploadMap(),
                deployConnect.getConnectId());
        LogUtil.info(EasyJenkinsEnum.SUCCESSFULLY_DEPLOYED.getParam(),deployConnect.getConnectId(),deployConnect.getHost());
    }
}
